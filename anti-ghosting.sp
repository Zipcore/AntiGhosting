#pragma semicolon 1

#define PLUGIN_VERSION "1.0"

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>

#include <smlib>

public Plugin myinfo = 
{
	name = "Anti Ghosting",
	author = ".#Zipcore",
	description = "",
	version = PLUGIN_VERSION,
	url = ""
};

//Specmodes
#define SPECMODE_NONE 					0
#define SPECMODE_FIRSTPERSON 			4
#define SPECMODE_3RDPERSON 				5
#define SPECMODE_FREELOOK	 			6

#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))
#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))

int g_iVelOff;

ConVar cvEnable;
bool g_bEnable;

public void OnPluginStart()
{
	CreateConVar("anti_ghosting_version", PLUGIN_VERSION, "No Weapon Fix Version", FCVAR_DONTRECORD|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	
	cvEnable = CreateConVar("anti_ghosting_enable", "1", "Set to 0 to disable this plugin.");
	g_bEnable = GetConVarBool(cvEnable);
	HookConVarChange(cvEnable, OnSettingChanged);
	
	HookUserMessage(GetUserMessageId("ProcessSpottedEntityUpdate"), Hook_ProcessSpottedEntityUpdate, true);
	
	LoopIngamePlayers(client)
		OnClientPutInServer(client);
}

public int OnSettingChanged(Handle convar, const char[] oldValue, const char[] newValue)
{
	if(convar == cvEnable)
		g_bEnable = view_as<bool>(StringToInt(newValue));
}

public void OnClientPutInServer(int client)
{
	if (g_iVelOff < 1)
		g_iVelOff = GetEntSendPropOffs(client, "m_vecVelocity[0]");
	
	SDKHook(client, SDKHook_SetTransmit, Hook_SetTransmit);
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if(g_bEnable)
		DoOurChecks(client);
	
	return Plugin_Continue;
}

stock bool DoOurChecks(int client)
{
	if(IsPlayerAlive(client))
		return;
	
	// Specmode
	int iSpecMode = GetEntProp(client, Prop_Send, "m_iObserverMode");
	if (iSpecMode == SPECMODE_FIRSTPERSON)
		SetEntProp(client, Prop_Send, "m_iObserverMode", SPECMODE_3RDPERSON);
}

public Action Hook_ProcessSpottedEntityUpdate(UserMsg msg_id, Handle bf, const int[] players, int playersNum, bool reliable, bool init)
{
	if(g_bEnable)
		return Plugin_Handled;
	else return Plugin_Continue;
}

public Action Hook_SetTransmit(int entity, int client)
{
	if(!g_bEnable)
		return Plugin_Continue;
		
	if(IsPlayerAlive(client))
		return Plugin_Continue;
		
	if(!Entity_IsPlayer(entity))
		return Plugin_Continue;
	
	if(GetClientTeam(entity) == CS_TEAM_T)
		return Plugin_Handled;
	
	return Plugin_Continue;
}

public void OnGameFrame()
{
	if(!g_bEnable)
		return;
	
	LoopAlivePlayers(i)
	{
		if(GetClientTeam(i) != CS_TEAM_T)
			continue;
		
		ChangeEdictState(i, g_iVelOff);
	}
}